#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

struct Person
{
	string name;
	string lastname;
	string ID;
	string Hrs;
	int pay;
	int gross;
};

void PrintPerson(Person* pPer)
{
	cout << "\n" << "Person" << "\n" << "First Name: " << pPer->name << "\n" << "Last Name: " << pPer->lastname << "\n" << "ID NUMBER: " << pPer->ID << "\n" << "Amount of Hours: " << pPer->Hrs << "\n" << "Pay rate: " << pPer->pay << "\n" << "Gross pay: " << pPer->gross << "\n\n\n";
}

int main()
{
	const int SIZE = 4;
	string IDs[SIZE] = { "The First's Person's four digit ID number", "The Second Person's four digit ID number", "The Third Person's four digit ID number", "The Fourth Person's four digit ID number" };
	string names[SIZE] = { "The First Person's First Name", "The Second Person's First Name", "The Third Person's First Name", "The Fourth Person's First Name" };
	string lastnames[SIZE] = { "The First Person's Last Name", "The Second Person's Last Name", "The Third Person's Last Name", "The Fourth Person's Last Name" };
	int Hours[SIZE] = {};
	int pay[SIZE] = {};
	int gross;

	for (int i = 0; i < SIZE; i++)
	{
		cout << "Enter " << names[i] << ": ";
		cin >> names[i];

	}
	for (int i = 0; i < SIZE; i++)
	{

		cout << "Enter " << lastnames[i] << ": ";
		cin >> lastnames[i];
	}
	for (int i = 0; i < SIZE; i++)
	{

		cout << "Enter " << IDs[i] << ": ";
		cin >> IDs[i];
	}
	for (int i = 0; i < SIZE; i++)
	{

		cout << "Enter each persons amount of hours from person 1 to 4" << ": ";
		cin >> Hours[i];
	}
	for (int i = 0; i < SIZE; i++)
	{

		cout << "Enter each persons hourly pay in order from person 1 to 4" << ": ";
		cin >> pay[i];
	}
	Person Persons;
	Persons.name = names[0];
	Persons.lastname = lastnames[0];
	Persons.Hrs = Hours[0];
	Persons.ID = IDs[0];
	Persons.pay = pay[0];
	Persons.gross = Hours[0] * pay[0];
	PrintPerson(&Persons);

	Person* pPerson = new Person;
	pPerson->name = names[1];
	pPerson->lastname = lastnames[1];
	pPerson->Hrs = Hours[1];
	pPerson->ID = IDs[1];
	pPerson->pay = pay[1];
	pPerson->gross = Hours[1] * pay[1];
	PrintPerson(pPerson);

	Person* pPersonss = new Person;
	pPersonss->name = names[2];
	pPersonss->lastname = lastnames[2];
	pPersonss->Hrs = Hours[2];
	pPersonss->ID = IDs[2];
	pPersonss->pay = pay[2];
	pPersonss->gross = Hours[2] * pay[2];
	PrintPerson(pPersonss);

	Person* pPersonsss = new Person;
	pPersonsss->name = names[3];
	pPersonsss->lastname = lastnames[3];
	pPersonsss->Hrs = Hours[3];
	pPersonsss->ID = IDs[3];
	pPersonsss->pay = pay[3];
	pPersonsss->gross = Hours[3] * pay[3];
	PrintPerson(pPersonsss);

	cout << "Total Employee  Gross Pay: " << (Hours[3] * pay[3]) + (Hours[2] * pay[2]) + (Hours[1] * pay[1]) + (Hours[0] * pay[0]) << "\n\n\n";

	_getch();
	return 0;
}